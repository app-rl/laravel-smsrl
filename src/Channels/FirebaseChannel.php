<?php

namespace Smsrl\Channels;

use Smsrl\Services\FirebaseService;
use Illuminate\Notifications\Notification;

class FirebaseChannel
{
	protected $firebaseService;

	public function __construct(FirebaseService $firebaseService)
	{
		$this->firebaseService = $firebaseService;
	}

	public function send($notifiable, Notification $notification)
	{
		if (method_exists($notifiable, 'routeNotificationForFirebase')) {
			$fcmTokens = $notifiable->routeNotificationForFirebase();
		}

		if ($notification->fcmTokens) {
			$fcmTokens = $notification->fcmTokens;
		}

		if (!method_exists($notification, 'toFirebase')) {
			return;
		}

		$message = method_exists($notification, 'toFirebase')
			? $notification->toFirebase($notifiable)
			: null;

		return $this->firebaseService->sendNotification($fcmTokens, $message);
	}
}
