<?php

namespace Smsrl\Services;

use Illuminate\Support\Facades\Http;

class Smsrl
{
    public static function send($phone, $text, $config = null, $method = 'POST')
    {
        if (! $config) {
            $config = config('smschannel');
        }

        if (! $config['user']) {
            return ['error' => 'User not configured'];
        }

        if ($config['disable']) {
            return ['error' => 'SMS sending is disabled'];
        }

        if (app()->environment() != 'production') {
            $phone = $config['test_phone'] ?? '';
        }

        $params = array_merge(
            collect($config)->only(['user', 'password'])->toArray(),
            compact('phone', 'text')
        );

        $response = Http::withOptions($config['curl'] ?? [])
            ->$method($config['server'], $params);

        // send and get json
        if ($response->successful()) {
            return $response->json();
        }
    }
}
