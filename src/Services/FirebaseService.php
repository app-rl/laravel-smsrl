<?php

namespace Smsrl\Services;

use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Exception\Messaging\FirebaseException;

class FirebaseService
{
    protected $messaging;

    public function __construct()
    {
        // Initialize Firebase Admin SDK using the credentials from the environment variable
        $firebaseCredentials = config('smschannel.firebase_credentials');

        // Create the Firebase factory using the credentials array instead of a path
        $factory = (new Factory)->withServiceAccount($firebaseCredentials);
        $this->messaging = $factory->createMessaging();
    }

    /**
     * Send a notification to multiple device tokens using Firebase Admin SDK
     *
     * @param array $deviceTokens
     * @param array $messageData
     * @return array
     */
    public function sendNotification(array $deviceTokens, array $messageData)
    {
        // Construct the notification
        $notification = Notification::create(
            $messageData['title'] ?? '',
            $messageData['body'] ?? ''
        );

        // Create the message payload
        $message = CloudMessage::new()
            ->withNotification($notification)
            ->withData($messageData['additional_data'] ?? []);

        // Handle additional options like image, click action, and priority
        if (isset($messageData['image'])) {
            $message = $message->withNotification(Notification::create(
                $messageData['title'] ?? '',
                $messageData['body'] ?? '',
                $messageData['image']
            ));
        }

        if (isset($messageData['click_action'])) {
            $message = $message->withData(['click_action' => $messageData['click_action']]);
        }

        if (isset($messageData['priority'])) {
            $message = $message->withAndroidConfig(['priority' => $messageData['priority']]);
        }

        // Send the notification using the multicast feature
        try {
            $response = $this->messaging->sendMulticast($message, $deviceTokens);
            return [
                'success' => $response->successes(),
                'failures' => $response->failures(),
            ];
        } catch (FirebaseException $e) {
            // Handle any errors that occur
            return ['error' => $e->getMessage()];
        }
    }
}
